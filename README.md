#True Colours Data Anonymizer
The True Colours Data Anonymizer is an excel add-in to anonymize data from True Colours.

## Contents

The True Colours Data Anonymizer consists of two files as follows:

* [`TC_Anonymizer.xlam`](https://bitbucket.org/amossstudy/truecoloursdataanonymizer/src/master/TC_Anonymizer.xlam) - This is the excel add-in file that performs the anonymization procedure.

* [`lookup.xlsx`](https://bitbucket.org/amossstudy/truecoloursdataanonymizer/src/master/lookup.xlsx) - This is an excel spreadsheet containing a lookup table of True Colours numbers to Anonymized IDs.

## Functionality

The tool supports two basic functions as follows:

* Anonymization of True Colours data to protect the privacy of participants.

* Export of True Colours data to CSV format for analysis.  

## How to use

### Opening the True Colours Data Anonymizer

1. Open the `TC_Anonymizer.xlam` file by double clicking on it.

  * When opeining the file, you may receive a warning as follows. Press 'Enable Macros' to continue.

    ![](https://bytebucket.org/amossstudy/truecoloursdataanonymizer/raw/master/img/security_warning.png)

2. This will add a new tab to excel as follows to provide access to the required functionality.

    ![](https://bytebucket.org/amossstudy/truecoloursdataanonymizer/raw/master/img/toolbar.png)

### Anonymizing True Colours Data

1. On the toolbar in the 'Add-Ins' tab press 'Load Lookup'.

2. In the open window, point to the `lookup.xlsx` file included with the anonymizer.

  * This will load the lookup table of True Colours Numbers to anonymised IDs.

  * To change the lookup, modify this file.

3. Open the data file of True Colours data.

4. On the toolbar in the 'Add-Ins' tab press 'Anonymize!'

  * This will go through the True Colours data file and replace any references to True Colours Numbers in the lookup table with the anonymized IDs.

  * Any fields that are changed will be highlighted for easy review.

### Exporting True Colours Data to CSV Files

1. On the toolbar in the 'Add-Ins' tab press 'Export to CSV'.

2. Browse to the location to export the .csv files.

  * Any previous exports will be overridden. 